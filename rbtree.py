#! /usr/bin/python
import networkx as nx
from matplotlib import pyplot as plt
MAX_NODE_NUM = 100



class Node(object):
    def __init__(self,i,v):
        self.v = v
        self.i = i # pointer
        self.p = None
        self.left = None
        self.right = None
        self.black = False

    def info(self):
        print 'NODES INFO:'
        print 'index:{},value:{},color:{},parent:{},left:{},right:{}'.\
            format(self.i,self.v,'BLACK' if self.black else 'RED', self.p,self.left,self.right)




class RBTree(object):
    def __init__(self):
        self.root = None
        self.nodes = [None]*MAX_NODE_NUM
        self.flag = False

    def _add_node(self,r,n):
        self.nodes[n.i] = n
        if r is None:
            self.root = n
        else:
            if r.v >= n.v and r.left is None:
                r.left = n.i
                n.p = r.i
                # print '{} added to the left of {}'.format(n.v,r.v)
            elif r.v>=n.v and r.left is not None:
                self._add_node(self.nodes[r.left],n)
            elif r.v < n.v and r.right is None:
                r.right = n.i
                n.p = r.i
                # print '{} added to the right of {}'.format(n.v,r.v)
            elif r.v < n.v and r.right is not None:
                self._add_node(self.nodes[r.right],n)

    def _left_rotation(self,X,Y):
        #         X
        #
        #   Y           Z
        # Change Y and X
        if Y.right is not None:
            self.nodes[Y.right].p = X.i
        if X.p is not None:
            if self.nodes[X.p].left == X.i:
                self.nodes[X.p].left = Y.i
            elif self.nodes[X.p].right == X.i:
                self.nodes[X.p].right = Y.i
        if X.p is None:
            self.root = Y
        X.left = Y.right
        Y.right = X.i
        Y.p = X.p
        X.p = Y.i

    def _right_rotation(self,X,Y):
        #         X
        #
        #   Z           Y
        # Change Y and X

        if Y.left is not None:
            self.nodes[Y.left].p = X.i
        if X.p is not None:
            if self.nodes[X.p].left == X.i:
                self.nodes[X.p].left = Y.i
            elif self.nodes[X.p].right == X.i:
                self.nodes[X.p].right = Y.i
        if X.p is None:
            self.root = Y
        X.right = Y.left
        Y.left = X.i
        Y.p = X.p
        X.p = Y.i

    def _case1(self):
        print 'case 1'
        self.Y.black = True
        self.Z.black = True
        self.X.black = False
        self.n = self.X

    def _case2(self):
        print 'case 2'
        self._left_rotation(self.X,self.Y)
        self.X.black = False
        self.Y.black = True
        self.n = self.Y

    def _case3(self):
        print 'case 3'
        self._right_rotation(self.Y,self.n)
        self.X.black = False
        self.n.black = True
        self.Z.black = True
        self.Y.black = False
        self.n = self.X

    def _case4(self):
        print 'case 4'
        self._right_rotation(self.Y,self.n)
        self._left_rotation(self.X,self.n)
        self.n.black = True
        self.Y.black = False
        self.X.black = False

    def _case5(self):
        print 'case 5'
        self.X.black = False
        self.Z.black = True
        self.Y.black = True
        self.n = self.X

    def _case6(self):
        print 'case 6'
        # self.plot_tree()
        self._right_rotation(self.X,self.Y)
        self.Y.black = True
        self.X.black = False
        self.n.black = False
        self.n = self.Y
        # self.plot_tree()

    def _case7(self):
        print 'case 7'
        self._left_rotation(self.Y,self.n)
        self.X.black = False
        self.Z.black = True
        self.n.black = True
        self.n = self.X

    def _case8(self):
        print 'case 8'
        self._left_rotation(self.Y,self.n)
        self._right_rotation(self.X,self.n)
        self.X.black = False
        self.Y.black = False
        self.n.black = True

    def _case9(self):
        print 'case 9'
        self.n = self.Y

    def _info(self):
         pass


    def rgtree_add_node(self,n):
        self.n = n
        self._add_node(self.root,self.n)
        self.root.black = True

        # self.plot_tree('Before modification,adding {}'.format(n.v))


        while self.n.p is not None:
            if self.n.black or self.nodes[self.n.p].black:
                break

            # raw_input('press any key to continue')


            self.Y = self.nodes[self.n.p]

            if self.Y.p is None:
                self._case9()
                self._info()
                continue
            else:
                self.X = self.nodes[self.Y.p]

            # case 1,2,3,4
            if self.X.left == self.Y.i:
                if (self.X.right is None) or self.nodes[self.X.right].black:
                    self.Z = None if self.X.right is None else self.nodes[self.X.right]
                    if self.Y.left == self.n.i:
                        self._case2()
                        self._info()
                        continue
                    elif self.Y.right == self.n.i:
                        self._case4()
                        self._info()
                        continue

                else:
                    self.Z = self.nodes[self.X.right]
                    if self.Y.left == self.n.i:
                        self._case1()
                        self._info()
                        continue
                    if self.Y.right == self.n.i:
                        self._case3()
                        self._info()
                        continue

            # case 5,6,7,8
            else:
                if (self.X.left is None) or self.nodes[self.X.left].black:
                    self.Z = None if self.X.left is None else self.nodes[self.X.left]
                    if self.Y.right == self.n.i:
                        self._case6()
                        self._info()
                        continue
                    elif self.Y.left == self.n.i:
                        self._case8()
                        self._info()
                        continue

                else:
                    self.Z = self.nodes[self.X.left]
                    if self.Y.right == self.n.i:
                        self._case5()
                        self._info()
                        continue
                    elif self.Y.left == self.n.i:
                        self._case7()
                        self._info()
                        continue
        # self.plot_tree('After modification,adding {}'.format(n.v))



    def add_node(self,n):
        self._add_node(self.root,n)
        # if n is root node
        if n.p == None:
            return

        n.black =  False
        # if n's parent is black
        if self.nodes[n.p].black is True:
            return

        Y = self.nodes[n.p]
        X = self.nodes[self.nodes[n.p].p]
        # if n's parent sibling is null
        if X.right is None:
            # print 'X.right is None'
            if Y.left == n.i:
                self._left_rotation(X,Y)
                Y.black = True
                X.black = False
                self.root.black = True
            else:
                self._right_rotation(Y,n)
                self._left_rotation(X,n)
                n.black = True
                X.black = False
                self.root.black = True
            return
        elif X.left is None:
            # print 'X.left is None'
            if Y.right == n.i:
                # print 'Y.right==n.i'
                self._right_rotation(X,Y)
                Y.black = True
                X.black = False
                self.root.black = True
            else:
                self._left_rotation(Y,n)
                self._right_rotation(X,n)
                n.black = True
                X.black = False
                self.root.black = True
            return



        Z_left_case = self.nodes[self.nodes[self.nodes[n.p].p].right]
        Z_right_case = self.nodes[self.nodes[self.nodes[n.p].p].left]

        # case 2.1 left: something wrong here --> if the parent of Y is also red
        if (Y.black is False) and \
                (X.left == Y.i) and \
                (Z_left_case.black is False):
            Y.black = True
            Z_left_case.black = True
            X.black = False


        # case 2.2 right:
        elif (Y.black is False) and \
                (X.right == Y.i) and \
                (Z_right_case.black is False):
            Y.black = True
            Z_right_case.black = True
            X.black = False

        # case 3.1 left
        elif (X.left == Y.i) and \
                (Y.black is False) and \
                (Z_left_case.black is True) and \
                (Y.left == n.i):
            # left rotation
            self._left_rotation(X,Y)

            # change color
            Y.black = True
            X.black = False
            Z_left_case.black = True
            self.root.black = True

        # case 3.2 right
        elif (X.right == Y.i) and \
                (Y.black is False) and \
                (Z_right_case.black is True) and \
                (Y.right == n.i):
            # right rotation
            self._right_rotation(X,Y)

            # change color
            Y.black = True
            X.black = False
            Z.black = True
            self.root.black = True

        # case 3.3 left + rotation
        elif (X.left == Y.i) and \
                (Y.black is False) and \
                (Z_left_case.black is True) and \
                (Y.right == n.i):
            self._left_rotation(X,Y)
            # left rotation
            self._left_rotation(X,n)

            # change color
            n.black = True
            X.black = False
            Z_left_case.black = True
            self.root.black = True

        # case 3.4 right + rotation
        elif (X.right == Y.i) and \
                (Y.black is False) and \
                (Z_right_case.black is True) and \
                (Y.left == n.i):
            self._right_rotation(X,Y)

            # right rotation
            self._right_rotation(X,n)

            # change color
            n.black = True
            X.black = False
            Z.black = True
            self.root.black = True

        return

    def _find_edges(self,n):
        if n == self.root:
            self.node_list = [self.root.i]
            self.edge_list = []
        if n.left is not None:
            # print 'Add {} to list, add {} to edges'.format(n.left,(n.i,n.left))
            self.node_list.append(n.left)
            self.edge_list.append((n.i,n.left))
            self._find_edges(self.nodes[n.left])
        if n.right is not None:
            # print 'Add {} to list, add {} to edges'.format(n.right,(n.i,n.right))
            self.node_list.append(n.right)
            self.edge_list.append((n.i,n.right))
            self._find_edges(self.nodes[n.right])


    def _extract_node_values(self):
        self.values = {}
        self.colors = [None]*len(self.node_list)
        for i in self.node_list:
            self.values[i] = self.nodes[i].v
            if self.nodes[i].black:
                self.colors[i] = 'k'
            else:
                self.colors[i] = 'r'
            if self.nodes[i].p is None:
                self.colors[i] = 'g'

    def plot_tree(self,title=''):
        if not self.root:
            # print 'There is only root'
            return None
        else:
            self._find_edges(self.root)
            G = nx.DiGraph()
            # print self.node_list
            G.add_nodes_from(self.node_list)
            G.add_edges_from(self.edge_list)
            self._extract_node_values()
            nx.draw_spring(G,arrows=True,with_labels=True,node_color=self.colors,font_color='w',labels=self.values)
            print title
            plt.show()


    def info(self):
        for n in self.nodes:
            if n:
                print 'NODES INFO:'
                parent = None if n.p is None else self.nodes[n.p].v
                left = None if n.left is None else self.nodes[n.left].v
                right = None if n.right is None else self.nodes[n.right].v
                print '{},{},parent:{},left:{},right:{}'.\
                    format(n.v,'BLACK' if n.black else 'RED', parent,left,right)

    def _isBinarySearchTree(self,n):
        if n.left is not None:
            if self.nodes[n.left].v > n.v:
                self.isBST = False
                return
            else:
                self._isBinarySearchTree(self.nodes[n.left])
        if n.right is not None:
            if self.nodes[n.right].v < n.v:
                self.isBST = False
                return
            else:
                self._isBinarySearchTree(self.nodes[n.right])
        return

    def isBinarySearchTree(self):
        self.isBST = True
        self._isBinarySearchTree(self.root)
        return self.isBST

    def _noredredpair(self,n):
        if n.left is not None:
            if (self.nodes[n.left].black == False) and (n.black == False):
                self.nrrpair = False
                return
            else:
                self._noredredpair(self.nodes[n.left])
        if n.right is not None:
            if (self.nodes[n.right].black == False) and (n.black == False):
                self.nrrpair = False
                return
            else:
                self._noredredpair(self.nodes[n.right])
        return

    def noredredpair(self):
        self.nrrpair = True
        self._noredredpair(self.root)
        if self.root.black is not True:
            self.nrrpair = False
        return self.nrrpair


    def isRBTree(self):
        if not self.noredredpair():
            return False
        if not self.isBinarySearchTree():
            return False

        return True



# test example

values = (3,7,10,14,17,26,12,15,16,19,21,20,23,28,30,41,38,35,39,47)
# values = (7,3,10,1,0,8,9)
# values = (3,7,10,14,17,26,12,15,16)

T = RBTree()
for i,v in enumerate(values):
    if v==16:
        T.flag = True
    n = Node(i,v)
    # # naive add nodes, may cause unbalanced trees --> test ok
    # T._add_node(T.root,n)

    # #
    T.rgtree_add_node(n)
    # T._add_node(T.root,n)

print T.isBinarySearchTree()
print T.isRBTree()
T.plot_tree()
# T.info()
# print T.root.v

















